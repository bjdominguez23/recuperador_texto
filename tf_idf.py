import math

def main():
    tdm = open('tdm_tf_idf.csv','w') #csv para la matriz tdm
    tes = open('tesauro.txt','r') #archivo que contiene el tesauro
    bag = open('bow_lemma.txt','r') #archivo con palabras,frecuencias,ntf 

    tesauro = tes.read().split('\n') #guardar las palabras en una lista
    tes.close()
    idf = {'Correo': 0} #Diccionario 
    for p in tesauro:
        idf[p] = 0
        
    frec = bag.read().split('I') #Guardar en una lista frec de cada correo
    tam = len(tesauro)
    bag.close()
    for f in frec:
        if len(f) > 0:
            frec_p = f.split('\n')
            if frec_p[0][0]==':':
                cont = 0
                for p in frec_p:
                    p = p.split(' ')
                    if p[0] == ':':
                        idf['Correo'] = idf['Correo'] + 1
                    elif len(p[0])>0:
                        idf[p[0]] = idf[p[0]] + 1
    tdm.write('Correo/Término,')
    for p in tesauro: #Obtener idf y escribir encabezado de la matriz
        if len(p)>0:
            idf[p] = math.fabs(math.log10(idf['Correo']/idf[p]))
            tdm.write(p + ',')
    tdm.write('\n')

    for f in frec: #llenar matriz 
        if len(f) > 0:
            frec_p = f.split('\n')
            if frec_p[0][0]==':':
                cont = 0
                for p in frec_p:
                    p = p.split(' ')
                    if p[0] == ':':
                        tdm.write(p[1]+',')
                    elif len(p[0])>0:
                        while p[0] != tesauro[cont]:
                            tdm.write('0,')
                            cont = cont + 1
                        tf_idf = int(p[1]) * idf[p[0]]                  
                        tdm.write(str(tf_idf) + ',')
                        cont = cont + 1;
                while cont < tam:
                    tdm.write('0,')
                    cont = cont + 1
                tdm.write('\n')
    tdm.close()
      
main()

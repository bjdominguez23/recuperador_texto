class AlgoritmoPorter:

    def consonante(self,palabra,pos):
        letra = palabra[pos]
        if pos>0:
            letra_ant = palabra[pos-1]
        if letra not in  'aeiou' and pos >0:
            if letra_ant not in 'aeiou' and letra == 'y':
                return False
            else:
                return True
        elif letra not in 'aeiou':
            return True
        else:
            return False
    
    #Obtener la m de [C](VC)^m[V]
    def num_m(self, palabra):
        vc = ''
        for i in range(len(palabra)):
            if self.consonante(palabra,i):
                if i==0:
                    vc = 'C'
                elif vc[-1] != 'C':
                        vc = vc + 'C'
            else:
                if i==0:
                    vc = 'V'
                elif vc[-1] != 'V':
                    vc = vc + 'V'
        m = vc.count('VC')
        return m

    #metodo para sustituir en la palabra alguna subcadena
    def sust(self,palabra,subcad,nueva):
        pos = palabra.rfind(subcad)
        palabra = palabra[:pos] + nueva
        return palabra

    #sustituye la terminacion si m>0
    def sust_m0(self,original,subcad,nueva):
        pos = original.rfind(subcad)
        base = original[:pos]
        if self.num_m(base) > 0:
            original = base + nueva
        return original
    
    #sustituye la terminacion si m>1
    def sust_m1(self,palabra,subcad,nueva):
        pos = palabra.rfind(subcad)
        base = palabra[:pos]
        if self.num_m(base) > 1:
            palabra = base + nueva
        return palabra
           

    #para ver si tiene la forma *v*
    def contieneVocal(self, palabra):
        for i in palabra:
            if i in 'aeiou':
                return True
        return False

    #para ver si tiene la forma *d
    def dobleConsonante(self,palabra):
        if len(palabra) >=2:
            if self.consonante(palabra,-1) and self.consonante(palabra,-2) and palabra[-1] == palabra[-2]:
                return True
            else:
                return False
        else:
            return False

    #para ver si tiene la forma *o
    def cvc(self,palabra):
        if len(palabra) >= 3:
            if self.consonante(palabra,-3) and self.consonante(palabra,-2) and self.consonante(palabra,-1):
                if palabra[-3] not in 'wxy':
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    
    def paso_1a(self,palabra):
        if palabra.endswith('sses'):
            palabra = self.sust(palabra,'sses','ss')
        if palabra.endswith('ies'):
            palabra = self.sust(palabra,'ies','i')
        if palabra.endswith('s'):
            palabra = self.sust(palabra,'s','')
        return palabra

    def paso_1b(self,palabra):
        regla2_3 = False; #si se usa la regla  2 o 3 aplican mas casos
        #EED -> ED si m>0
        if palabra.endswith('eed'):
            pos = palabra.rfind('eed')
            base = palabra[:pos]
            if self.num_m(base) > 0:
                palabra = base + 'ee'
        elif palabra.endswith('ed'): #ED ->
            pos = palabra.rfind('ed')
            base = palabra[:pos]
            if self.contieneVocal(base):
                palabra = base
                regla2_3 = True
        elif palabra.endswith('ing'): #ING ->
            pos = palabra.rfind('ing')
            base = palabra[:pos]
            if self.contieneVocal(base):
                palabra = base
                regla2_3 = True
        if regla2_3:
            if palabra.endswith('at') or palabra.endswith('bl') or palabra.endswith('iz'):
                palabra += 'e'
            elif self.dobleConsonante(palabra) and not palabra.endswith('l') and not palabra.endswith('s') and not palabra.endswith('z'):
                palabra = palabra[:-1]
            elif self.num_m(palabra) == 1 and self.cvc(palabra):
                palabra += 'e'
        return palabra                          
    
    def paso_1c(self,palabra):
        if palabra.endswith('y') and self.contieneVocal(palabra[:-1]):
            palabra = self.sust(palabra,'y','i')
        return palabra

    def paso_2(self,palabra):
        if palabra.endswith('ational'):
            palabra = self.sust_m0(palabra,'ational','ate')
        elif palabra.endswith('tional'):
            palabra = self.sust_m0(palabra,'tional','tion')
        elif palabra.endswith('enci'):
            palabra = self.sust_m0(palabra,'enci','ence')
        elif palabra.endswith('anci'):
            palabra = self.sust_m0(palabra,'anci','ance')
        elif palabra.endswith('izer'):
            palabra = self.sust_m0(palabra,'izer','ize')
        elif palabra.endswith('abli'):
            palabra = self.sust_m0(palabra,'abli','able')
        elif palabra.endswith('alli'):
            palabra = self.sust_m0(palabra,'alli','al')
        elif palabra.endswith('entli'):
            palabra = self.sust_m0(palabra,'entli','ent')
        elif palabra.endswith('eli'):
            palabra = self.sust_m0(palabra,'eli','e')
        elif palabra.endswith('ousli'):
            palabra = self.sust_m0(palabra,'ousli','ous')
        elif palabra.endswith('ization'):
            palabra = self.sust_m0(palabra,'ization','ize')
        elif palabra.endswith('ation'):
            palabra = self.sust_m0(palabra,'ation','ate')
        elif palabra.endswith('ator'):
            palabra = self.sust_m0(palabra,'ator','ate')
        elif palabra.endswith('alism'):
            palabra = self.sust_m0(palabra,'alism','al')
        elif palabra.endswith('iveness'):
            palabra = self.sust_m0(palabra,'iveness','ive')
        elif palabra.endswith('fulness'):
            palabra = self.sust_m0(palabra,'fulness','ful')
        elif palabra.endswith('ousness'):
            palabra = self.sust_m0(palabra,'ousness','ous')
        elif palabra.endswith('aliti'):
            palabra = self.sust_m0(palabra,'aliti','al')
        elif palabra.endswith('iviti'):
            palabra = self.sust_m0(palabra,'iviti','ive')
        elif palabra.endswith('biliti'):
            palabra = self.sust_m0(palabra,'biliti','ble')
        return palabra

    def paso_3(self,palabra):
        if palabra.endswith('icate'):
            palabra = self.sust_m0(palabra,'icate','ic')
        elif palabra.endswith('ative'):
            palabra = self.sust_m0(palabra,'ative','')
        elif palabra.endswith('alize'):
            palabra = self.sust_m0(palabra,'alize','al')
        elif palabra.endswith('iciti'):
            palabra = self.sust_m0(palabra,'iciti','ic')
        elif palabra.endswith('ical'):
            palabra = self.sust_m0(palabra,'ical','ic')
        elif palabra.endswith('ful'):
            palabra = self.sust_m0(palabra,'ful','')
        elif palabra.endswith('ness'):
            palabra = self.sust_m0(palabra,'ness','')
        return palabra

    def paso_4(self,palabra):
        if palabra.endswith('al'):
            palabra = self.sust_m1(palabra,'al','')
        elif palabra.endswith('ance'):
            palabra = self.sust_m1(palabra,'ance','')
        elif palabra.endswith('ence'):
            palabra = self.sust_m1(palabra,'ence','')      
        elif palabra.endswith('er'):
            palabra = self.sust_m1(palabra,'er','')
        elif palabra.endswith('ic'):
            palabra = self.sust_m1(palabra,'ic','')
        elif palabra.endswith('able'):
            palabra = self.sust_m1(palabra,'able','')
        elif palabra.endswith('ible'):
            palabra = self.sust_m1(palabra,'ible','')
        elif palabra.endswith('ant'):
            palabra = self.sust_m1(palabra,'ant','')
        elif palabra.endswith('ement'):
            palabra = self.sust_m1(palabra,'ement','')    
        elif palabra.endswith('ment'):
            palabra = self.sust_m1(palabra,'ment','')
        elif palabra.endswith('ent'):
            palabra = self.sust_m1(palabra,'ent','')
        elif palabra.endswith('ou'):
            palabra = self.sust_m1(palabra,'ou','')
        elif palabra.endswith('ism'):
            palabra = self.sust_m1(palabra,'ism','')
        elif palabra.endswith('ate'):
            palabra = self.sust_m1(palabra,'ate','')
        elif palabra.endswith('iti'):
            palabra = self.sust_m1(palabra,'iti','')
        elif palabra.endswith('ous'):
            palabra = self.sust_m1(palabra,'ous','')
        elif palabra.endswith('ive'):
            palabra = self.sust_m1(palabra,'ive','')
        elif palabra.endswith('ize'):
            palabra = self.sust_m1(palabra,'ize','')
        elif palabra.endswith('ion'):
            pos = palabra.rfind('ion')
            base = palabra[:pos]                
            if self.num_m(base)>1 and base.endswith('s') or base.endswith('t'):
                palabra = base
            palabra = self.sust_m1(palabra,'','')
        return palabra

    def paso_5a(self,palabra):
        if palabra.endswith('e'):
            base = palabra[:-1]
            if self.num_m(palabra) > 1:
                palabra = base
            elif self.num_m(base) == 1 and not self.cvc(base):
                palabra = base
        return palabra

    def paso_5b(self,palabra):
        if self.num_m(palabra) > 1 and self.dobleConsonante(palabra) and palabra.endswith('l'):
            palabra = palabra[:-1]
        return palabra

    def lematizador(self,palabra):
        palabra = self.paso_1a(palabra)
        palabra = self.paso_1b(palabra)
        palabra = self.paso_1c(palabra)
        palabra = self.paso_2(palabra)
        palabra = self.paso_3(palabra)
        palabra = self.paso_4(palabra)
        palabra = self.paso_5a(palabra)
        palabra = self.paso_5b(palabra)
        return palabra

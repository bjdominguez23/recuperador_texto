import csv
import math
import operator
import AlgoritmoPorter
from AlgoritmoPorter import AlgoritmoPorter as porter
from collections import Counter

stop_words = open("stopwords.txt","r") #archivo de stopwords
tes = open('tesauro.txt','r') #archivo que contiene el tesauro
tesauro = tes.read().split('\n') #guardar las palabras en una lista
spam=csv.reader(open('spam.csv')) #abrimos el archivo spam
lista = open('lista.txt','r') #archivo que tiene la tdm como lista

palabras=[]
fitness = []
stop = stop_words.read().split('\n') #Guardar las stop words en una lista
lista_tdm = lista.read().split('I') #Separar la lista por documento
print("Ingresa una consulta: ")
consulta = input();
res = "".join((char if char.isalpha() else " ") for char in consulta).split()
for i in res:
    palabras.append(i) #poner las palabras en una lista para procesarlas
    palabras=[palabra.lower() for palabra in palabras] #convertir a minusculas para procesar correctamente
    palabras = [p for p in palabras if p not in stop] #Eliminar stopwords

#lematizar las palabras
for i,palabra in enumerate(palabras):
    palabras[i] = porter().lematizador(palabra)

palabras.sort()
#Contar las apariciones de cada palabra
frec = Counter(palabras);

for doc in lista_tdm:
    if len(doc) > 0:
        tf_idf = doc.split('\n')
        if tf_idf[0][0]==':':
            suma = 0
            raiz_q = 0
            raiz_d = 0
            fit = 0
            for p in tf_idf:
                palabra = p.split(' ')
                if palabra[0] != ':' and len(palabra[0])>0:
                    frec_q = frec[palabra[0]]
                    suma = suma + (float(palabra[1])*float(frec_q))
                    raiz_d = raiz_d + ((float(palabra[1]))*(float(palabra[1])))
                    raiz_q = raiz_q + (frec_q*frec_q)
            if (raiz_d != 0) and (raiz_q != 0):
                fit = suma/(math.sqrt(raiz_d) * math.sqrt(raiz_q))
            fitness.append(fit)
fitness = list(enumerate(fitness,1))
fitness.sort(key=operator.itemgetter(1), reverse = True)
spam = list(spam)
for pos,sim in fitness:
    if sim != 0:
        print(spam[pos][1] + ' ' + str(sim))
                                                
        

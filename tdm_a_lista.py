import csv

matriz_tdm = csv.reader(open('tdm_tf_idf.csv'))
lista = open("lista.txt","w")
tes = open('tesauro.txt','r') #archivo que contiene el tesauro
tesauro = tes.read().split('\n') #guardar las palabras en una lista
tes.close()

for row in matriz_tdm:
    if not row[1].isalpha():
        lista.write('I: ' + row[0] + '\n')
        #print(row[0])
        for i in range(1,len(row)-1):
            if row[i] != '0':
                lista.write(tesauro[i-1] + ' ' + row[i] + '\n')
                #print(row[i])
        lista.write('\n')


lista.close();

import csv #para abrir el pdf
import re
import os
import AlgoritmoPorter
from AlgoritmoPorter import AlgoritmoPorter as porter
#from Index import Index as index
from collections import Counter
spam=csv.reader(open('spam.csv')) #abrimos el archivo spam
stop_words = open("stopwords.txt","r") #archivo de stopwords
    
def main():
    cont = 1;
        
    palabras=[] #vector para guardar las palabras
    tesauro = []
    stop = stop_words.read().split('\n') #Guardar las stop words en una lista
    lemma = open('bow_lemma.txt',"w") #archivo para guardar terminos lematizados
    for reg in spam:
        if reg[0]=="spam" or reg[0]=="ham": #si es ham o spam 
            res="".join((char if char.isalpha() else " ") for char in reg[1]).split() #para separar la cadena en palabra eliminando los signos de puntuacion
            for i in res:
                palabras.append(i) #poner las palabras en una lista para procesarlas
            palabras=[palabra.lower() for palabra in palabras] #convertir a minusculas para procesar correctamente
            palabras = [p for p in palabras if p not in stop] #Eliminar stopwords

            #lematizar las palabras
            for i,palabra in enumerate(palabras):
                palabras[i] = porter().lematizador(palabra)

            palabras.sort() #ordenar en olden alfabetico
            #Contar apariciones de cada palabra
            frec = Counter(palabras)
            mayor = frec.most_common(1) #Termino que aparece más veces
            frec = list(frec.items())

            
            lemma.write('I: '+ str(cont) +'\n')
            for i in frec:
                lemma.write(i[0] + ' ' + str(i[1]) + ' ' + ("%.4f"%(i[1]/mayor[0][1])) + '\n' ) #Guardas la palabra, frecuencia y NTF
                tesauro.append(i[0]) #Guardar palabra en el tesauro
            lemma.write('\n')
            cont = cont + 1
            palabras.clear() #vaciar la lista

    tesauro = list(set(tesauro)) #Eliminar terminos repetidos del tesauro
    tesauro.sort() #ordenar el tesauro
    tes = open('tesauro.txt','w')
    for i in tesauro:
        tes.write(i+'\n')
    tes.close()
    lemma.close()

    #obtener matriz termino-documento
    #index().matriz_tdm()
main()

def main():
    tdm = open('matriz_tdm.csv','w') #csv para la matriz tdm
    tes = open('tesauro.txt','r') #archivo que contiene el tesauro
    bag = open('bow_lemma.txt','r') #archivo con palabras,frecuencias,ntf 

    tesauro = tes.read().split('\n') #guardar las palabras en una lista
    tes.close()
    tdm.write('Correo/Término,')
    for p in tesauro: #encabezado de la matriz
        tdm.write(p+',')
    tdm.write('\n')

    frec = bag.read().split('I') #Guardar en una lista frec de cada correo
    tam = len(tesauro)
    bag.close()
    for f in frec:
        if len(f) > 0:
            frec_p = f.split('\n')
            if frec_p[0][0]==':':
                cont = 0
                for p in frec_p:
                    p = p.split(' ')
                    if p[0] == ':':
                        tdm.write(p[1]+',')
                    elif len(p[0])>0:
                        while p[0] != tesauro[cont]:
                            tdm.write('0,')
                            cont = cont + 1
                        tdm.write(p[2] + ',')
                        cont = cont + 1;
                while cont < tam:
                    tdm.write('0,')
                    cont = cont + 1
                tdm.write('\n')
    tdm.close()
main()
